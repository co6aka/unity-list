﻿using UnityEngine;

public class Mallet : MonoBehaviour
{
    public Texture2D cursorTexture;
    private Vector2 cursorHotspot;

    void Start()
    {
        if (cursorTexture != null)
        {
            cursorHotspot = new Vector2(0f, cursorTexture.height * 0.66f);  // left lower third is the hammer's striking area
            Cursor.SetCursor(cursorTexture, cursorHotspot, CursorMode.Auto);
        }
    }
}
