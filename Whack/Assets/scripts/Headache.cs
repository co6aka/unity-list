﻿using UnityEngine;
using System.Collections;

public class Headache : MonoBehaviour {

    public ParticleSystem sparks;    

    // no collider- just a mouse hit
    void OnMouseUpAsButton() {

        print("Ouch!");        

        if (sparks != null) {
            Instantiate(sparks, transform.position, Quaternion.identity);
        }        
    }

}