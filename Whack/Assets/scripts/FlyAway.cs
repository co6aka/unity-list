﻿using UnityEngine;
using System.Collections;

public class FlyAway : MonoBehaviour {
	
	private Vector3 flyDirection = new Vector3(-1f, 1f, 0f);        // towards upper-left corner
    private Rigidbody2D butterfly;

	// Use this for initialization
	void Start () {
        butterfly = GetComponent<Rigidbody2D>();
        Invoke("flyAway", 2f);
    }
	
    /////// wait a few seconds, then fly away
	void flyAway () {
        butterfly.velocity = flyDirection;
	}
}
