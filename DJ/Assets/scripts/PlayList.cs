﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayList : MonoBehaviour {

    // set up single or multiple AudioClip's to play

    private AudioSource player; 

    void Start()
    {
        player = gameObject.AddComponent<AudioSource>();
    }

    public void Next()
    {
       // ...
    }

    public void Prev()
    {
        // ...
    }

    public void Play()
    {
        player.clip = null;     // pop current selection into player
        player.Play();
    }

    public  void Stop()
    {
        player.Stop();
    }

}
